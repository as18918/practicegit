// C# program to print Hello World! 
using System; 
  
// namespace declaration 
namespace HelloWorldApp { 
      
    // Class declaration 
    class ZS_OE { 
          
        // Main Method 
        static void Main(string[] args) { 
              
            // statement 
            // printing Hello World! 
            Console.WriteLine("Hello cha nge the  World!"); 
              
            // To prevents the screen from  
            // running and closing quickly 
            Console.ReadKey(); 
        } 
    } 
}